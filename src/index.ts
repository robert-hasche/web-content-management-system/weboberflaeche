import express, { Request, Response } from "express";
import hbs from "express-handlebars";
import path from "path";
import fs from "fs";

console.log("Starting Express App for Web Content Management");

let app = express();

//Set static resources
app.use(express.static(`${__dirname}/public`));

//Set Handlebars Engine
app.engine("hbs", hbs({
    extname: "hbs",
    defaultLayout: "default",
    layoutsDir: `${__dirname}/views/layouts/`,
    partialsDir: `${__dirname}/views/partials/`
}));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

//Set Entrypoint for Application
app.get("/", (req: Request, res: Response) => {
    res.render("main", {
        title: "Titel",
        navListEntries: JSON.parse(fs.readFileSync(`${__dirname}/example-json-files/navListExample.json`).toString())
    });
});

console.log("App has been started");
app.listen(80);