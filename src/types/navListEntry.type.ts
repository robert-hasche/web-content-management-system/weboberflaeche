import { navList } from "./navList.type";

export type navListEntry = {
    title: string;
    link?: string;
    subNavList?: navList;
}